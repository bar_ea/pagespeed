// Set modules
let fs = require('fs');
let os = require('os');
let path = require('path');

// Get parameters
let config = require(__dirname + '/settings/config');
let resultFile = path.join(__dirname, config.dir.result, config.resultFilename+'.csv');

fs.writeFileSync(resultFile, 'Web-site,Mobile,Desktop' + os.EOL, {encoding: 'utf-8', flag: 'w'});

// Check pulled results
fs.readdir(path.join(__dirname, config.dir.result), (err, files)=>{
  if (err) {console.log(err);}
  files = files.filter((item)=>{
    return item.search(/\.html$/)>-1;
  });
  files.forEach((item)=>{
    getSiteInfoSync(item);
  });
});



function getSiteInfoSync(aitem){
  let siteName = aitem.slice(0,-5);
  console.log(siteName + ' is proceeding');
  let data, row;

  data = fs.readFileSync(path.join(__dirname, config.dir.result, aitem), {encoding: 'utf-8'});
  let prodRgxp = /<div class="lh-gauge__percentage">(\d+)<\/div>/g;
  let efficiency = data.match(prodRgxp);
  
  if (efficiency!==null && efficiency.length==2) {
    let level=[];
    level[0] = efficiency[0].match(/\d+/)[0];
    level[1] = efficiency[1].match(/\d+/)[0];
    if ((+level[0] <= +config.speedLevel)||(+level[1] <= +config.speedLevel)){
      row = siteName + ',' + level[0] + ',' + level[1] + os.EOL;
      fs.writeFileSync(resultFile, row, {encoding: 'utf-8', flag: 'as'});
    }
  } else {
      row = siteName + ',?,?' + os.EOL;
      fs.writeFileSync(resultFile, row, {encoding: 'utf-8', flag: 'as'});
  }
}