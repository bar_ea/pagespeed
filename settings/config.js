// settings for pagespeed.js
let params = {
  // файл-источник данных
  src: './settings/pagespeed.txt',
  // файл результата
  resultFilename: 'result',
  // кодировка файлов
  encoding: 'utf-8',
  // уровень оптимизации. Сайты с оптимизацией ниже этого будут попадать в файл результатов
  speedLevel: 100,
  // Если параметр выставлен в true, будет запущен видимый экземпляр nightmare - можно заварить кофе и представить, что ты через RDP наблюдаешь как кто-то другой работает за тебя
  showBrowser: false,
  delay: {
    nightmare: 35000,
    to: 30000,
    // временная задержка для Гугла, чтобы он успел дать ответ
    processUrl: 25000
  },
  dir: {
    // папка для сохранения результатов
    result: './result/'
  }
}

module.exports = params;