// Set modules
const { rejects } = require('assert');
let fs = require('fs');
let path = require('path');


// Get parameters
let param = require(__dirname + '/settings/config');

let cntr = 0;

  let myCodes;
  
  fs.readFile(__dirname + '/' + param.src, param.encoding, (err, data) => {
    if (err) {console.log(err);}
    myCodes = data.split('\n'); 
  });
  
  let to = setTimeout( function tick(){
    let to = setTimeout(tick, param.delay.to);

    if(cntr>=myCodes.length){
      setTimeout(()=>{console.log('Script finished!')}, 1000);
      clearTimeout(to);
      return;
    }

    checkUrl(myCodes[cntr]);
    cntr++;
    console.log('cntr is:' + cntr + ' ' + myCodes.length);
  }, 1000);

 async function checkUrl(aurl){
  let httpRgxp = /http(?:s)?\:\/\/(.+)/;
  let promise = new Promise((resolve, reject)=>{

    let nightmareInstance = {};
    nightmareInstance.show = param.showBrowser;
    nightmareInstance.waitTimeout = param.delay.nightmare;

    const Nightmare = require('nightmare');
    const nightmare = Nightmare(nightmareInstance);

    let savePath = path.join(__dirname, param.dir.result,);
    let savePathMobile = path.join(__dirname, param.dir.result, 'mobile');
    let savePathDesktop = path.join(__dirname, param.dir.result, 'desktop');
    let siteName = aurl.match(httpRgxp)[1];
    console.log(aurl + ' in process...');
    nightmare
    .goto(`https://developers.google.com/speed/pagespeed/insights/?hl=ru&url=${encodeURIComponent(aurl)}&tab=mobile`)
    .wait(param.delay.processUrl)
    .html(savePath + siteName + '.html', 'HTMLComplete')
    .end()
    .then(()=>{
      console.log(aurl + ' is ready');
      resolve(aurl);
    })
    .catch(error => {
      console.error('Search failed:', error);
    });

    });
    return await promise;
  }